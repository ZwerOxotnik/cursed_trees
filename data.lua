--[[
Copyright 2019-2020 ZwerOxotnik <zweroxotnik@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
]]--

-- You can write and receive any information on the links below.
-- Source: https://gitlab.com/ZwerOxotnik/cursed_trees
-- Mod portal: https://mods.factorio.com/mod/cursed_trees

local type_data = data.raw["unit"]

local function get_enitity(name)
	return type_data[name]
end

-- biters
local biters = {}
biters.small = get_enitity("small-biter")
biters.medium = get_enitity("medium-biter")
biters.big = get_enitity("big-biter")
biters.behemoth = get_enitity("behemoth-biter")

-- spitters
local spitters = {}
spitters.small = get_enitity("small-spitter")
spitters.medium = get_enitity("medium-spitter")
spitters.big = get_enitity("big-spitter")
spitters.behemoth = get_enitity("behemoth-spitter")

-- spawners
local spawners = {}
type_data = data.raw["unit-spawner"]
spawners.biter = get_enitity("biter-spawner")
spawners.spitter = get_enitity("spitter-spawner")

-- trees
local trees = {}
type_data = data.raw["tree"]
trees.t01 = util.table.deepcopy(type_data["tree-01"])
trees.t02 = util.table.deepcopy(type_data["tree-02"])
trees.t02_red = util.table.deepcopy(type_data["tree-02-red"])
trees.t03 = util.table.deepcopy(type_data["tree-03"])
trees.t04 = util.table.deepcopy(type_data["tree-04"])
trees.t05 = util.table.deepcopy(type_data["tree-05"])
trees.t06 = util.table.deepcopy(type_data["tree-06"])
trees.t06_brown = util.table.deepcopy(type_data["tree-06-brown"])
trees.t07 = util.table.deepcopy(type_data["tree-07"])
trees.t08 = util.table.deepcopy(type_data["tree-08"])
trees.t08_brown = util.table.deepcopy(type_data["tree-08-brown"])
trees.t08_red = util.table.deepcopy(type_data["tree-08-red"])
trees.t09 = util.table.deepcopy(type_data["tree-09"])
trees.t09_brown = util.table.deepcopy(type_data["tree-09-brown"])
-- trees.dry_tree = util.table.deepcopy(type_data["dry-tree"])
-- trees.dead_tree_desert = util.table.deepcopy(type_data["dead-tree-desert"])
-- trees.dead_grey_trunk = util.table.deepcopy(type_data["dead-grey-trunk"])
-- trees.dead_dry_hairy_tree = util.table.deepcopy(type_data["dead-dry-hairy-tree"])
-- trees.dry_hairy_trees = util.table.deepcopy(type_data["dry-hairy-trees"])

-- worms
local worms = {}
type_data = data.raw["turret"]
worms.medium_worm_turret = get_enitity("medium-worm-turret")
worms.big_worm_turret = get_enitity("big-worm-turret")
worms.behemoth_worm_turret = get_enitity("behemoth-worm-turret")

local function get_tree_animation(target, scale)
	local animations = {}
	local function add_layer(variation)
		table.insert(animations, util.table.deepcopy(variation))
		local layer = animations[#animations]
		layer.direction_count = 1
		layer.lines_per_file = 1
		layer.line_length = 1
		layer.slice = 1
		layer.scale = layer.scale or 1 * scale
		local hr_version = layer.hr_version
		if hr_version then
			hr_version.scale = hr_version.scale or 1 * scale
			hr_version.direction_count = 1
			hr_version.lines_per_file = 1
			hr_version.line_length = 1
			hr_version.slice = 1
		end
	end

	local variations = target.variations
	add_layer(variations[1].trunk)
	---- WIP
	-- add_layer(variations[1].leaves)
	-- if variations[1].shadow then
	-- 	add_layer(variations[1].shadow)
	-- 	local frame_count = animations[3].frame_count
	-- 	animations[1].frame_count = frame_count
    --     if animations[1].hr_version then
	-- 		animations[1].hr_version.frame_count = frame_count
	-- 	end
	-- 	animations[2].frame_count = frame_count
    --     if animations[2].hr_version then
	-- 		animations[2].hr_version.frame_count = frame_count
	-- 	end
	-- else
	-- 	local frame_count = animations[2].frame_count
	-- 	animations[1].frame_count = frame_count
    --     if animations[1].hr_version then
	-- 		animations[1].hr_version.frame_count = frame_count
	-- 	end
	-- end

	-- log(serpent.block(animations))

	return animations
end

local function change_resistances(resistances)
	if (not resistances) then return nil end
	local new_resistances = {}

	for _, resistance in ipairs(resistances) do
		if resistance.type ~= "fire" then
			local new_resistance = {
				type = resistance.type,
				decrease = resistance.decrease,
				percent = resistance.percent
			}
			if resistance.type == "explosion" then
				new_resistance.percent = resistance.percent or 0 + 20
			elseif resistance.type == "physical" then
				log(serpent.block(resistance))
				new_resistance.percent = resistance.percent or 0 + 30
			end
			table.insert(new_resistances, new_resistance)
		end
	end

	return new_resistances
end

local function change_collision(collision_box, scale)
	-- if (not collision_box) then return nil end

	local new_collision_box = {{1, 1},{1, 1}}
	new_collision_box[1][1] = collision_box[1][1] * scale
	new_collision_box[1][2] = collision_box[1][2] * scale
	new_collision_box[2][1] = collision_box[2][1] * scale
	new_collision_box[2][2] = collision_box[2][2] * scale

	return new_collision_box
end

local function change_collisions(paste_entity, copy_entity, scale)
	paste_entity.collision_box = change_collision(copy_entity.collision_box, scale)
	paste_entity.selection_box = copy_entity.selection_box
	paste_entity.map_generator_bounding_box = change_collision(copy_entity.drawing_box, scale)
end

local function change_prototype(paste_entity, copy_entity, scale)
	change_collisions(paste_entity, copy_entity, scale)
	paste_entity.resistances = change_resistances(paste_entity.resistances)
	paste_entity.dying_explosion = nil
    paste_entity.working_sound = nil
    paste_entity.dying_sound = nil
    paste_entity.secondary_animation = nil
    paste_entity.prepared_alternative_sound = nil
    paste_entity.starting_attack_sound = nil
    paste_entity.preparing_sound = nil
    paste_entity.prepared_sound = nil
	paste_entity.folding_sound = nil
	paste_entity.alternative_attacking_frame_sequence = nil
	paste_entity.localised_name = {'entity-name.' .. copy_entity.name}
	paste_entity.corpse = copy_entity.remains_when_mined
	if paste_entity.attack_parameters then
		paste_entity.attack_parameters.sound = nil
	end
end

local function change_biter(biter, tree, scale)
	if scale == nil then scale = 1 end
	change_prototype(biter, tree, scale)
	local animation = get_tree_animation(tree, scale)
	-- biter.max_health = biter.max_health * 1.2
	if biter.healing_per_tick > 0 then
		biter.healing_per_tick = biter.healing_per_tick * 0.8
	end
    biter.movement_speed = biter.movement_speed * 0.8
    biter.vision_distance = biter.vision_distance * 0.8
	biter.run_animation.layers = animation
	biter.attack_parameters.animation.layers = animation
	biter.idle_animation = {}
	biter.idle_animation.layers = animation

	-- local frame_sequence = biter.alternative_attacking_frame_sequence
	-- if frame_sequence then
	-- 	frame_sequence.warmup_frame_sequence = {1}
	-- 	frame_sequence.warmup2_frame_sequence = {1}
	-- 	frame_sequence.attacking_frame_sequence = {1}
	-- 	frame_sequence.cooldown_frame_sequence = {1}
	-- 	frame_sequence.prepared_frame_sequence = {1}
	-- 	frame_sequence.back_to_walk_frame_sequence = {1}
	-- end
end

-- TODO: Fix sprites
local function change_spawner(spawner, tree, scale)
	if scale == nil then scale = 1 end
	change_prototype(spawner, tree, scale)
	local animation = get_tree_animation(tree, scale)
	-- spawner.max_health = spawner.max_health * 1.2
	if spawner.healing_per_tick > 0 then
		spawner.healing_per_tick = spawner.healing_per_tick * 0.8
	end
    spawner.animations = animation
end

local function change_worm(worm, tree, scale)
	if scale == nil then scale = 1 end
	change_prototype(worm, tree, scale)
	local animation = get_tree_animation(tree, scale)
	-- worm.max_health = worm.max_health * 1.2
	if worm.healing_per_tick > 0 then
		worm.healing_per_tick = worm.healing_per_tick * 0.8
	end
    worm.folding_animation.layers = animation
    worm.folded_animation.layers = animation
    worm.preparing_animation.layers = animation
    worm.prepared_animation.layers = animation
    worm.starting_attack_animation.layers = animation
    worm.prepared_alternative_animation.layers = animation
    worm.folding_animation.layers = animation
    worm.ending_attack_animation.layers = animation
	worm.integration = nil

	-- log(serpent.block(worm))
    -- WIP
end

change_spawner(spawners.biter, trees.t08_brown, 5)
change_spawner(spawners.spitter, trees.t08_red, 5)

change_biter(biters.small, trees.t01)
change_biter(biters.medium, trees.t02, 1.5)
change_biter(biters.behemoth, trees.t02_red, 1.8)
change_biter(biters.big, trees.t03, 2)

change_biter(spitters.small, trees.t04)
change_biter(spitters.medium, trees.t05, 1.5)
change_biter(spitters.big, trees.t06, 1.8)
change_biter(spitters.behemoth, trees.t06_brown, 2)

-- change_worm(worms.medium_worm_turret, trees.t06, 2)
-- change_worm(worms.big_worm_turret, trees.t04, 3)
-- change_worm(worms.behemoth_worm_turret, trees.t06_brown, 4)

--TODO: change attack
--TODO: change spawners background
